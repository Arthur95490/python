def main():
    e = int(input('Écris un nombre entier'))
    i = 2
    while i < e and e % i != 0:
        i = i + 1
    if i == e:
        print(e, "est premier")
    else:
        print("Ce n'est pas un nombre premier.")
if __name__ == '__main__':
    main()