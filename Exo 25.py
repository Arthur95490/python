def main():
    texte = input("Entrez une chaine de caractères ")
    textereverse = ""
    i = len(texte) - 1
    while i >= 0:
        textereverse += texte[i]
        i -= 1
    print(textereverse)

if __name__ == '__main__':
    main()