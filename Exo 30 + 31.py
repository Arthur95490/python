def main():
    list = [1, 2, 3, 4, 5, 6]
    lp = []
    lu = []
    for i in range(len(list)):
        if i % 2 == 0:
            lp.append(i)
        else:
            lu.append(i)
    print("Ces nombres sont pairs", lp, "     Ces nombres sont impairs", lu)


if __name__ == '__main__':
    main()