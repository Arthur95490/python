def main():
    d = int(input("Choisissez le nombre de votre choix"))
    if int(str(d)) ** 2 == d:
        print("Le nombre est un carré parfait")
    else:
        print("Le nombre n'est pas un carré parfait")

if __name__ == '__main__':
    main()
