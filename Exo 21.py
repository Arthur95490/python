def main():
    chaine = input("Ecrire une chaine de caractère")
    liste = ["a", "e", "i", "o", "u", "y"]
    nbv = 0
    for i in range(len(chaine)):
        if chaine[i] in liste:
            nbv += 1
    if nbv == 0:
        print("Il n'y a pas de voyelles dans la chaine " + chaine)
    else:
        print("La chaine de caractère " + chaine + " contient " + str(nbv) + " voyelles.")

if __name__ == '__main__':
    main()
