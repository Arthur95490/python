from math import *

def main():
    r = int(input("Choissez un rayon pour votre cercle"))
    peri = int(2 * pi * r)
    aire = int(pi * r ** 2)
    print(peri)
    print(aire)

if __name__ == '__main__':
    main()