from collections import Counter
def main():
    chaine = "itescia"
    freq = Counter(chaine)
    for (a, b) in freq.items():
        print("Le caractère ", a, " figure ", b," fois dans la chaine")
if __name__ == '__main__':
    main()