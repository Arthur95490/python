def main():
    chaine = input("Mettre une chaine de caractere")
    l = chaine.split()
    mot = ""
    for i in l:
        if (len(mot) < len(i)):
            mot = i
    print("Le mot le plus long est", mot)

if __name__ == '__main__':
    main()