def main():
    texte = input("Ecrire un mot")
    for i in range(len(texte) // 2):
        if texte[i] != texte[-i - 1]:
            a = 1
        else:
            a = 0

    if a == 0:
        print("C'est un palindrome")
    else:
        print("Ce n'est pas un palindrome")

if __name__ == '__main__':
    main()