def main():
    a = []
    n = 1
    for i in range(1, 10):
        print(n, " x ", i, " = ", n * i)

    for j in range(1, 10):
        for i in range(1, 10):
            print(j, " x ", i, " = ", j * i)

if __name__ == '__main__':
    main()