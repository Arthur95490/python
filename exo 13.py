def main():
    b = int(input("Choissez votre chiffre a diviser"))
    c = int(input("Choissez votre diviseur"))
    quotient = int(b // c)
    reste = int(b % c)
    print("Le quotient de ", b, " et ", c, "est égale à", quotient, "et le reste est de ", reste)

if __name__ == '__main__':
    main()
