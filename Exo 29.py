def main():
    list1 = [1, 2, 3, 4]

    list2 = [2, 4, 6, 8]

    list1set = set(list1)

    intersection = list1set.intersection(list2)

    interlist = list(intersection)

    print("les valeurs communes sont", interlist)

if __name__ == '__main__':
    main()
