def main():
    chaine = input("Entrez une chaine de caractère")
    for i in range(len(chaine)):
        if (chaine[i] == "a"):
            print("Le caractère 'a' se trouve à la position : ", i)

if __name__ == '__main__':
    main()