def main():
    def decorateur(func):
        def helper(*args, **argv):
            helper.calls += 1
            return func(*args, **argv)

        helper.calls = 0
        return helper

    @decorateur
    def decorer(x):
        return x + 1

    if __name__ == '__main__':
        print(decorer(0))
        print(decorer(1))
        print("number of calls:", decorer.calls)

if __name__ == '__main__':
    main()